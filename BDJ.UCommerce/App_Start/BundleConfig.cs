﻿using System.Web;
using System.Web.Optimization;

namespace BDJ.UCommerce
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            //bundles.UseCdn = true;
            //var jqueryCdnPath = "https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.1.min.js";
            //bundles.Add(new ScriptBundle("~/bundles/jquery", jqueryCdnPath).Include(
            //            "~/Scripts/jquery-{version}.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
               "~/Scripts/jquery-3.3.1.min.js"
               ));
            bundles.Add(new ScriptBundle("~/bundles/dataTable").Include(
               "~/Scripts/datatables.min.js"
               ));
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                       "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
            bundles.Add(new StyleBundle("~/Content/font-awesome").Include(
                "~/Content/Template/font-awesome/css/font-awesome.css"));
            bundles.Add(new StyleBundle("~/Template/css").Include(
                "~/Content/Template/css/bootstrap-responsive.min.css",
                "~/Content/Template/css/matrix-login.css",
                "~/Content/Template/css/matrix-media.css",
                "~/Content/Template/css/matrix-style.css",
                "~/Content/Template/css/custom.css"
                   ));
            bundles.Add(new ScriptBundle("~/Template/js").Include(
                "~/Content/Template/js/matrix.js",
                "~/Content/Template/js/matrix.login.js",
                "~/Content/Template/js/matrix.popover.js",
                "~/Content/Template/js/matrix.table.js",
                "~/Content/Template/js/matrix.dashboard.js"
                ));
            BundleTable.EnableOptimizations = true;
        }
    }
}
