﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BDJ.UCommerce.Models;

namespace BDJ.UCommerce.Controllers
{
    public class CategoryController : Controller
    {
        private bdjUCommerceEntities db = new bdjUCommerceEntities();

        // GET: Category
        public ActionResult Index()
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "Category";
                return RedirectToAction("Login", "Users");
            }
            return View();
        }
        public ActionResult LoadData()
        {
            var categories = db.spCategoryView();
            return Json(new { data = categories }, JsonRequestBehavior.AllowGet);
        }

        // GET: Category/Details/5
        public ActionResult Details(int? id)
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "Category";
                return RedirectToAction("Login", "Users");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            category category = db.categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // GET: Category/Create
        public ActionResult Create()
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "create";
                Session["dc"] = "Category";
                return RedirectToAction("Login", "Users");
            }
            ViewBag.categoryId = new SelectList(db.categories, "id", "name");
            return View();
        }

        // POST: Category/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,description,categoryId")] category category)
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "create";
                Session["dc"] = "Category";
                return RedirectToAction("Login", "Users");
            }
            if (ModelState.IsValid)
            {
                db.categories.Add(category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.categoryId = new SelectList(db.categories, "id", "name", category.categoryId);
            return View(category);
        }

        // GET: Category/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "Category";
                return RedirectToAction("Login", "Users");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            category category = db.categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            ViewBag.categoryId = new SelectList(db.categories, "id", "name", category.categoryId);
            return View(category);
        }

        // POST: Category/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,description,categoryId")] category category)
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "Category";
                return RedirectToAction("Login", "Users");
            }
            if (ModelState.IsValid)
            {
                db.Entry(category).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.categoryId = new SelectList(db.categories, "id", "name", category.categoryId);
            return View(category);
        }

        // GET: Category/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "Category";
                return RedirectToAction("Login", "Users");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            category category = db.categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // POST: Category/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "Category";
                return RedirectToAction("Login", "Users");
            }
            category category = db.categories.Find(id);
            db.categories.Remove(category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
