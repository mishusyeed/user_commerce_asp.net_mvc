﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BDJ.UCommerce.Models;

namespace BDJ.UCommerce.Controllers
{
    [Authorize]
    public class ProductLikeController : Controller
    {
        private bdjUCommerceEntities db = new bdjUCommerceEntities();
        
        // GET: ProductLike
        public ActionResult Index()
        {
            var productLikes = db.productLikes.Include(p => p.product).Include(p => p.user);
            return View(productLikes.ToList());
        }

        // GET: ProductLike/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            productLike productLike = db.productLikes.Find(id);
            if (productLike == null)
            {
                return HttpNotFound();
            }
            return View(productLike);
        }

        // GET: ProductLike/Create
        public ActionResult Create()
        {
            ViewBag.ProductId = new SelectList(db.products, "Id", "Name");
            ViewBag.UserId = new SelectList(db.users, "Id", "Name");
            return View();
        }

        // POST: ProductLike/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ProductId,UserId,DateTime,Ip")] productLike productLike)
        {
            if (ModelState.IsValid)
            {
                db.productLikes.Add(productLike);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProductId = new SelectList(db.products, "Id", "Name", productLike.ProductId);
            ViewBag.UserId = new SelectList(db.users, "Id", "Name", productLike.UserId);
            return View(productLike);
        }

        // GET: ProductLike/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            productLike productLike = db.productLikes.Find(id);
            if (productLike == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductId = new SelectList(db.products, "Id", "Name", productLike.ProductId);
            ViewBag.UserId = new SelectList(db.users, "Id", "Name", productLike.UserId);
            return View(productLike);
        }

        // POST: ProductLike/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ProductId,UserId,DateTime,Ip")] productLike productLike)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productLike).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProductId = new SelectList(db.products, "Id", "Name", productLike.ProductId);
            ViewBag.UserId = new SelectList(db.users, "Id", "Name", productLike.UserId);
            return View(productLike);
        }

        // GET: ProductLike/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            productLike productLike = db.productLikes.Find(id);
            if (productLike == null)
            {
                return HttpNotFound();
            }
            return View(productLike);
        }

        // POST: ProductLike/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            productLike productLike = db.productLikes.Find(id);
            db.productLikes.Remove(productLike);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
