﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BDJ.UCommerce.Models;

namespace BDJ.UCommerce.Controllers
{
    public class BrandController : Controller
    {
        private bdjUCommerceEntities db = new bdjUCommerceEntities();

        // GET: Brand
        public ActionResult Index()
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "Brand";
                return RedirectToAction("Login", "Users");
            }
            return View();
        }
        public ActionResult LoadData()
        {
            var data = db.brands.Select(i => new { i.id, i.name, i.description, i.origin }).ToList();
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        // GET: Brand/Details/5
        public ActionResult Details(int? id)
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "Brand";
                return RedirectToAction("Login", "Users");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            brand brand = db.brands.Find(id);
            if (brand == null)
            {
                return HttpNotFound();
            }
            return View(brand);
        }

        // GET: Brand/Create
        public ActionResult Create()
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "create";
                Session["dc"] = "Brand";
                return RedirectToAction("Login", "Users");
            }
            return View();
        }

        // POST: Brand/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,description,origin")] brand brand)
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "create";
                Session["dc"] = "Brand";
                return RedirectToAction("Login", "Users");
            }
            if (ModelState.IsValid)
            {
                db.brands.Add(brand);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(brand);
        }

        // GET: Brand/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "Brand";
                return RedirectToAction("Login", "Users");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            brand brand = db.brands.Find(id);
            if (brand == null)
            {
                return HttpNotFound();
            }
            return View(brand);
        }

        // POST: Brand/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,description,origin")] brand brand)
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "Brand";
                return RedirectToAction("Login", "Users");
            }
            if (ModelState.IsValid)
            {
                db.Entry(brand).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(brand);
        }

        // GET: Brand/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "Brand";
                return RedirectToAction("Login", "Users");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            brand brand = db.brands.Find(id);
            if (brand == null)
            {
                return HttpNotFound();
            }
            return View(brand);
        }

        // POST: Brand/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "Brand";
                return RedirectToAction("Login", "Users");
            }
            brand brand = db.brands.Find(id);
            db.brands.Remove(brand);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
