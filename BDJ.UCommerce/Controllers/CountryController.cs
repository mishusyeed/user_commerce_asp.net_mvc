﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BDJ.UCommerce.Models;

namespace BDJ.UCommerce.Controllers
{
    public class CountryController : Controller
    {
        private bdjUCommerceEntities db = new bdjUCommerceEntities();

        // GET: Country
        public ActionResult Index()
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "Country";
                return RedirectToAction("Login", "Users");
            }
            return View();
        }
        public ActionResult LoadData()
        {
            var data = db.spCountryView();
            return Json(new { data =  data }, JsonRequestBehavior.AllowGet);
        }

        // GET: Country/Details/5
        public ActionResult Details(int? id)
        {

            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "Country";
                return RedirectToAction("Login", "Users");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            country country = db.countries.Find(id);
            if (country == null)
            {
                return HttpNotFound();
            }
            return View(country);
        }

        // GET: Country/Create
        public ActionResult Create()
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "create";
                Session["dc"] = "Country";
                return RedirectToAction("Login", "Users");
            }
            return View();
        }

        // POST: Country/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name")] country country)
        {

            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "create";
                Session["dc"] = "Country";
                return RedirectToAction("Login", "Users");
            }
            if (ModelState.IsValid)
            {
                db.countries.Add(country);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(country);
        }

        // GET: Country/Edit/5
        public ActionResult Edit(int? id)
        {

            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "Country";
                return RedirectToAction("Login", "Users");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            country country = db.countries.Find(id);
            if (country == null)
            {
                return HttpNotFound();
            }
            return View(country);
        }

        // POST: Country/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name")] country country)
        {

            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "Country";
                return RedirectToAction("Login", "Users");
            }
            if (ModelState.IsValid)
            {
                db.Entry(country).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(country);
        }

        // GET: Country/Delete/5
        public ActionResult Delete(int? id)
        {

            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "Country";
                return RedirectToAction("Login", "Users");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            country country = db.countries.Find(id);
            if (country == null)
            {
                return HttpNotFound();
            }
            return View(country);
        }

        // POST: Country/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "Country";
                return RedirectToAction("Login", "Users");
            }
            country country = db.countries.Find(id);
            db.countries.Remove(country);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
