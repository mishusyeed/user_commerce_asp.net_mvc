﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using BDJ.UCommerce.Models;

namespace BDJ.UCommerce.Controllers
{

    public class HomeController : Controller
    {
        bdjUCommerceEntities db = new Models.bdjUCommerceEntities();
        // GET: Home
        public ActionResult Index()
        {
            var data = (from pi in db.productImages
                        orderby pi.Id descending
                        select pi).Take(5).ToList();

            return View(data);
        }
       public ActionResult About()
        {
            return View();
        }
        
    }
}