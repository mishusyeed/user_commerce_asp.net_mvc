﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BDJ.UCommerce.Models;

namespace BDJ.UCommerce.Controllers
{
    public class CityController : Controller
    {
        private bdjUCommerceEntities db = new bdjUCommerceEntities();

        // GET: City
        public ActionResult Index()
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "City";
                return RedirectToAction("Login", "Users");
            }
            var cities = db.cities.Include(c => c.country);
            return View(cities.ToList());
        }
        public ActionResult LoadData()
        {
            var data = db.spCityView();
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        // GET: City/Details/5
        public ActionResult Details(int? id)
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Index";
                Session["dc"] = "City";
                return RedirectToAction("Login", "Users");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            city city = db.cities.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            return View(city);
        }

        // GET: City/Create
        public ActionResult Create()
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Create";
                Session["dc"] = "City";
                return RedirectToAction("Login", "Users");
            }
            ViewBag.countryId = new SelectList(db.countries, "id", "name");
            return View();
        }

        // POST: City/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,countryId")] city city)
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "Create";
                Session["dc"] = "City";
                return RedirectToAction("Login", "Users");
            }
            if (ModelState.IsValid)
            {
                db.cities.Add(city);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.countryId = new SelectList(db.countries, "id", "name", city.countryId);
            return View(city);
        }

        // GET: City/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "index";
                Session["dc"] = "City";
                return RedirectToAction("Login", "Users");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            city city = db.cities.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            ViewBag.countryId = new SelectList(db.countries, "id", "name", city.countryId);
            return View(city);
        }

        // POST: City/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,countryId")] city city)
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "index";
                Session["dc"] = "City";
                return RedirectToAction("Login", "Users");
            }
            if (ModelState.IsValid)
            {
                db.Entry(city).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.countryId = new SelectList(db.countries, "id", "name", city.countryId);
            return View(city);
        }

        // GET: City/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "index";
                Session["dc"] = "City";
                return RedirectToAction("Login", "Users");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            city city = db.cities.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            return View(city);
        }

        // POST: City/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["type"].ToString() != "admin")
            {
                Session["dv"] = "index";
                Session["dc"] = "City";
                return RedirectToAction("Login", "Users");
            }
            city city = db.cities.Find(id);
            db.cities.Remove(city);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
